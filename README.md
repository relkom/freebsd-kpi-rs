# The project was terminated on 06.11.2024.
# DO NOT REPORT ANY ISSUES!

# FreeBSD 14.0-RELEASE Rust Kernel Programming Interface ![KPI logo](https://gitlab.com/4neko/freebsd-kpi-rs/-/raw/master/logo_600.png?ref_type=heads&inline=true)

**This repository contains only bindings to the kernel!**

**It is unsafe, just a bindings!**

**It it still not tested (because structure alignment tests are now performed, see [Kernel Programming Interface Test](https://gitlab.com/relkom/freebsd-kpi-test/-/tree/releng/14.0))**

**For the 'safe' and rust compatiable realization see [Kernel Module Interface](https://gitlab.com/relkom/freebsd-kmod-kpi)**

`Kernel Module Interface` depends on this crate.

---

## Variables (Features autoconfigure)

In order to build crate against the FreeBSD's kernel `conf` file (located in /usr/src/sys/<`arch`>/conf) in order to autocinfigre features
- disable `default_features`
- set feature `BUILD_CONFIG_KERNEL`
- set env variable `KPI_PLATFORM_CFG_PATH`

i.e
```toml
[dependencies]
freebsd-kpi-r14-0 = { path = "../rsandbox_kmod", defailt_features = false, features = ["BUILD_CONFIG_KERNEL"] }

[env]
KPI_PLATFORM_CFG_PATH = "/usr/src/sys/amd64/conf/GENERIC"
```

If `KPI_PLATFORM_CFG_PATH` was not set then a deafult path `/usr/src/sys/amd64/conf/GENERIC` to `GENERIC` will be used!

## Porting guide

Please refer to [The Rustonomicon](https://doc.rust-lang.org/nomicon/ffi.html)

### Private Function
```Rust
/// defined in <path to file with filename>:line number
#[repr(C)]
pub struct turnstile 
{
    _data: [u8; 0],
    _marker:
        core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}
```

### Extern functions and variables
```Rust
extern "C"
{
    #[no_mangle]
    pub 
    fn vmem_create(name: *const c_char, base: vmem_addr_t, size: vmem_size_t, 
        quantum: vmem_size_t, qcache_max: vmem_size_t, flags: c_int) -> *mut vmem_t;

    #[no_mangle]
    #[link_name = "wdog_software_attach"]
    pub static mut wdog_software_attach: Option<unsafe extern "C" fn()>;
}
```

### Pointer to function
A function fointer like 
```C
void (*func)()
```
is:
```Rust
Option<unsafe extern "C" fn()>
```

### Functions
A special attributes
- C `__returns_twice`, Rust `#[ffi_returns_twice]`
- C `__no_return`, Rust `!`
- C `__weak_symbol`, Rust #[linkage = "extern_weak"]

### Other
- `volatile` arguments or fields should have comment `/// VOLATILE!` and accessed and
accessed using  `core::ptr::read_volatile`  and `core::ptr::write_volatile`
- C `offsetof` for rust there is a macros: `offsetof.rs` `offset_of!()`
- C `typeof` there is no alternative for the Rust.

---

## Styles

Some guidelins on formatting and porting.

Do not use `rustfmt`!.

### Line breaks and formatting.

Normally the length of the line should be no more than 120 chars. But it is ok up to 160.

Function fromatting:
```Rust
// #[ATTRIBUTES] \n
// #[ATTRIBUTES] \n
// [FUNCTION MODIFIERS] \n
// [FUNCTION] [FUNCTION_NAME] [GENERICS][ARGUMENTS] [RETURN_DATA]\n
// [{]
//      [...]
// [}]
//
// where [ARGUMENTS] may be formatted as following:
// ([ARG], [ARG], [ARG])
// -- OR --
// (
//    [ARG],
//    [ARG]
// )
// -- OR --
// ([ARG], [ARG], [ARG],
//  [ARG], [ARG])

#[inline]
pub unsafe
fn function_name<B, C, D>(arg1: u64, arg2: u64, arg3: B, arg4: C, arg5: D) -> u64
where B: SomeTrait, C: SomeTrait, D: SomeTrait
{}

#[inline]
pub unsafe
fn function_name<B, C, D>(
    arg1: u64, 
    arg2: u64,
    arg3: B,
    arg4: C,
    arg5: D
) -> u64
where B: SomeTrait, C: SomeTrait, D: SomeTrait
{}

#[inline]
pub unsafe
fn function_name<B, C, D>(arg1: u64, arg2: u64, 
    arg3: B, arg4: C, arg5: D) -> u64
where   // optiinal \n 
    B: SomeTrait, C: SomeTrait, D: SomeTrait
{}
```

Const/type fromatting:
```Rust
// [VISIBILITY] [CONST] [NAME][:] [DATATYPE] [=] [DATA]
// [VISIBILITY] [TYPE] [TYPE_NAME] = [TYPE]

pub const RW_VAL: u8 = 9;
const D_VAL: &[u8] = b"d_value\0";
const B_VAL: &[&[u8]] = & // \n if list if large, otherwise same line
[
    D_VAL,
    "blabla\0",
];

pub type uint32_t = u32;
pub type callback_fn = // \n if long difinition
    Option<
        unsafe extern "C"
        fn(my_var: u64) -> u64
    >;

pub type callback_fn = unsafe extern "C" fn(my_var: u64) -> u64;
pub type callback_fn = Option<unsafe extern "C" fn() -> u64>;
```

Assignment:
```Rust
//...
let var: u64 = some_struct.read().unwrap();

let var2: Option<Arc<RefCell<MyStructLongName>>> = 
    some_s.get_instance().get_sourcer().read().unwrap();

let var3: Arc<RefCell<MyStructLongName>> =
    io_source.instance()
            .grab()
            .set_source()
            .set_name()
            .start();
//...
```

Structs:
```Rust
//[DERIVES/MODIFIERS]
//[VISIBILITY][struct][name] \n
//[{] \n
//\t    [field][:] [data][,]
//[}]\n

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct StructName
{
    field: u64,
    field1: Option<Arc<Mutex<MyData>>>,
}
```

Match/If:
```Rust
if var1 == 8
{

}
else
{

}

if var1 == 8 || var_with_long_name > 6 ||
    var_with_anothr_name > 100 ||
    var5 != 8
{

}
else if var2 < 6
{

}
else
{

}

match myvar
{
    3 => return 5,
    6 =>
    {
        something
    },
    9 => return 1,
    _ =>
}

match myvar.get_instance().foo()
    .bar().foobar()
{
    StateA0V1H3
        {
            field1: ...,
            field2: ...,
            field3: ...
        } =>
    {

    },
    StateA4N6(a,b,c,d,e) =>
    {

    },
    StateA5K3B6R3blabla =>
    {

    }
}
```


## Security

I am trying my best to control what is pushed to this repository. No one else is allowed to commit to this repo! Mostly this crate contains structures. But sometimes '.h' files contains some static functions. A code in such functions must match with what in the originam '.h' file! Otherwise, any changes must be highlighted and a reason for changes must be in the comment sections. Also, the author of the changes must be mentioned!

## Requests

Request to implement something or to fix something and ASAP are rejected.

Only if sponsored.
